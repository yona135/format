/****************************************************
* Creator: Yehonatan Ben Avraham and Shira Kutzuker *
* From: Ashkelon, 11k.                              *
****************************************************/

#include <string>
#include <cstdarg>		// Documentation can be found at: https://www.cplusplus.com/reference/cstdarg/.
#include <iostream>

using std::cout;
using std::string;

/// <summary> 
/// The function will format a string by the agreed sign '{}'. 
/// </summary>
/// <param name="main">
/// The main string, the pattern in which the other obtained parameters are embedded.
///  </param>
/// Hence the amount of parameters is not limited, the programmer is responsible for matching
/// the number of parameters to the number of agreed signs in order for the function to behave as expected.
/// <returns>
/// The format string. 
/// </returns>
string format(string main, ...)
{
	int index = 0;
	va_list words;
	va_start(words, main);

	while ((index = main.find("{}")) != string::npos)
	{
		main.replace(index, 2, va_arg(words, string));
	}

	va_end(words);
	return main;
}

int main()
{
	string word = "world";
	cout << format("Hello {}!", word);
	return 0;
}

// OUTPUT: Hello world!